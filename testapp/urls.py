from django.conf.urls import patterns, include, url
from django.contrib import admin
from someapp.views import HelloView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'testapp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', HelloView.as_view()),
)
