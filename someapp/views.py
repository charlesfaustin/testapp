from django.shortcuts import render
from django.views.generic import TemplateView
# Create your views here.


class HelloView(TemplateView):
    template_name = 'hello.html'
